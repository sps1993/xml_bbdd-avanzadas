package Dominio;

import java.util.LinkedList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

class MyHandler extends DefaultHandler {
	WriteXMLFile xml;
	LinkedList<String>els=new LinkedList<String>();
    private StringBuilder acc;
    boolean flagUri, flagSubject, flagContent, flagMaincat, flagBestAnswer, flagAnswerItem, flagCat, flagSubcat, flagDate, flagResDate, flagVotDate, flagLastAnswerTS, flagQLang, flagQINTL, flagLanguage, flagID, flagBestID;
	Question pregunta;
    public MyHandler() {
    	//xml=new CreateXML();
    	xml=new WriteXMLFile();
        acc = new StringBuilder();
        flagUri = false;
    	flagSubject = false;
    	flagContent = false;
    	flagMaincat = false;
    	flagBestAnswer = false;
    	flagAnswerItem = false;
    	flagCat = false;
    	flagSubcat = false;
    	flagDate = false;
    	flagResDate = false;
    	flagVotDate = false;
    	flagLastAnswerTS = false;
    	flagQLang = false;
    	flagQINTL = false;
    	flagLanguage = false;
    	flagID = false;
    	flagBestID = false;
    }
    
    public void startDocument() {
		System.out.println("Comienzo del Documento XML");
	}

	public void endDocument() {
		System.out.println("Final del Documento XML");
		//xml.crearXML(preguntas);
		xml.guardar();
	}
	
	public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
		if (qName.equalsIgnoreCase("uri")) {
			flagUri = true;
		}else{	
			if (qName.equalsIgnoreCase("subject")) {
				flagSubject = true;
			}else{	
				if (qName.equalsIgnoreCase("content")) {
					flagContent = true;
				}else{	
					if (qName.equalsIgnoreCase("bestanswer")){
						flagBestAnswer = true;
					}else{	
						if (qName.equalsIgnoreCase("answer_item")){
							flagAnswerItem=true;
						}else{
							if (qName.equalsIgnoreCase("cat")){
								flagCat = true;
							}else{
								if (qName.equalsIgnoreCase("maincat")) {
									flagMaincat = true;
								}else{
									if (qName.equalsIgnoreCase("subcat")){
										flagSubcat = true;
									}else{
										if (qName.equalsIgnoreCase("date")){
											flagDate = true;
										}else{
											if (qName.equalsIgnoreCase("res_date")){
												flagResDate = true;
											}else{
												if (qName.equalsIgnoreCase("vot_date")){
													flagVotDate = true;
												}else{
													if (qName.equalsIgnoreCase("lastanswerts")){
														flagLastAnswerTS = true;
													}else{
														if (qName.equalsIgnoreCase("qlang")){
															flagQLang = true;
														}else{
															if (qName.equalsIgnoreCase("qintl")){
																flagQINTL = true;
															}else{
																if (qName.equalsIgnoreCase("language")){
																	flagLanguage = true;
																}else{
																	if (qName.equalsIgnoreCase("id")){
																		flagID = true;
																	}else{
																		if(qName.equalsIgnoreCase("best_id")){
																			flagBestID = true;
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}	
		}
	}

    public void endElement(String uri, String localName, String qName)
            throws SAXException {
    	if (qName.equalsIgnoreCase("uri")) {
			pregunta=new Question();
			pregunta.setUri(acc.toString());
		}else{	
			if (qName.equalsIgnoreCase("subject")) {
				pregunta.setSubject(acc.toString());
			}else{	
				if (qName.equalsIgnoreCase("content")) {
					pregunta.setContent(acc.toString());
				}else{	
					if (qName.equalsIgnoreCase("bestanswer")){
						pregunta.setBestAnswer(acc.toString());
					}else{	
						if (qName.equalsIgnoreCase("answer_item")){
							els=pregunta.getAnswerItem();
							els.add(acc.toString());
							pregunta.setAnswerItem(els);
						}else{
							if (qName.equalsIgnoreCase("cat")){
								pregunta.setCat(acc.toString());
							}else{
								if (qName.equalsIgnoreCase("maincat")) {
									pregunta.setMaincat(acc.toString());
								}else{
									if (qName.equalsIgnoreCase("subcat")){
										pregunta.setSubcat(acc.toString());
									}else{
										if (qName.equalsIgnoreCase("date")){
											pregunta.setDate(acc.toString());
										}else{
											if (qName.equalsIgnoreCase("res_date")){
												pregunta.setResDate(acc.toString());
											}else{
												if (qName.equalsIgnoreCase("vot_date")){
													pregunta.setVotDate(acc.toString());
												}else{
													if (qName.equalsIgnoreCase("lastanswerts")){
														pregunta.setLastAnswerTS(acc.toString());
													}else{
														if (qName.equalsIgnoreCase("qlang")){
															pregunta.setqLang(acc.toString());
														}else{
															if (qName.equalsIgnoreCase("qintl")){
																pregunta.setqINTL(acc.toString());
															}else{
																if (qName.equalsIgnoreCase("language")){
																	pregunta.setLanguage(acc.toString());
																}else{
																	if (qName.equalsIgnoreCase("id")){
																		pregunta.setId(acc.toString());
																	}else{
																		if(qName.equalsIgnoreCase("best_id")){
																			pregunta.setBestID(acc.toString());
																			if(pregunta.getMaincat()!=null && ((pregunta.getMaincat().indexOf("Music")!=-1))){
																				//System.out.println(pregunta.getMaincat());
																				xml.create(pregunta);
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}	
		}
        acc.setLength(0);
    }

    public void characters(char[] ch, int start, int length)
            throws SAXException {
        acc.append(ch, start, length);
		//System.out.println("Maincat : " + new String(ch, start, length));
        if (flagUri) {
			flagUri = false;
		}
        if (flagSubject) {
			flagSubject = false;
		}
        if (flagContent) {
			flagContent = false;
		}
        if (flagBestAnswer){
			flagBestAnswer = false;
		}
        if (flagAnswerItem){
			flagAnswerItem=false;
        }
		if (flagCat){
			flagCat = false;
		}
		if (flagMaincat) {
			flagMaincat = false;
		}
		if (flagSubcat){
			flagSubcat = false;
		}
		if (flagDate){
			flagDate = false;
		}
		if (flagResDate){
			flagResDate = false;
		}
		if (flagVotDate){
			flagVotDate = false;
		}
		if (flagLastAnswerTS){
			flagLastAnswerTS = false;
		}
		if (flagQLang){
			flagQLang = false;
		}
		if (flagQINTL){
			flagQINTL = false;
		}
		if (flagLanguage){
			flagLanguage = false;
		}
		if (flagID){
			flagID = false;
		}if(flagBestID){
			flagBestID = false;
		}
    }
}