package Dominio;

import java.util.LinkedList;

public class Question {
	String uri, subject, content, maincat, bestAnswer, cat, subcat, date, resDate, votDate, lastAnswerTS, qLang, qINTL, language, id, bestID;
	LinkedList<String> answerItem;
	public Question(){
		answerItem = new LinkedList<String>();
	}
	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getMaincat() {
		return maincat;
	}

	public void setMaincat(String maincat) {
		this.maincat = maincat;
	}

	public String getBestAnswer() {
		return bestAnswer;
	}

	public void setBestAnswer(String bestAnswer) {
		this.bestAnswer = bestAnswer;
	}

	public LinkedList<String> getAnswerItem() {
		return answerItem;
	}

	public void setAnswerItem(LinkedList<String> answerItem) {
		this.answerItem = answerItem;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public String getSubcat() {
		return subcat;
	}

	public void setSubcat(String subcat) {
		this.subcat = subcat;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getResDate() {
		return resDate;
	}

	public void setResDate(String resDate) {
		this.resDate = resDate;
	}

	public String getVotDate() {
		return votDate;
	}

	public void setVotDate(String votDate) {
		this.votDate = votDate;
	}

	public String getLastAnswerTS() {
		return lastAnswerTS;
	}

	public void setLastAnswerTS(String lastAnswerTS) {
		this.lastAnswerTS = lastAnswerTS;
	}

	public String getqLang() {
		return qLang;
	}

	public void setqLang(String qLang) {
		this.qLang = qLang;
	}

	public String getqINTL() {
		return qINTL;
	}

	public void setqINTL(String qINTL) {
		this.qINTL = qINTL;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBestID() {
		return bestID;
	}

	public void setBestID(String bestID) {
		this.bestID = bestID;
	}
}
