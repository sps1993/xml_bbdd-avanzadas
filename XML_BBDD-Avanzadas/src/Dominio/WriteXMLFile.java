package Dominio;
import java.io.FileWriter;
import java.io.IOException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
public class WriteXMLFile {
	Element raiz, cuestion, respuestas;
	Document doc;
	public WriteXMLFile(){
		raiz = new Element("preguntas");
		doc = new Document(raiz);
		doc.setRootElement(raiz);
	}
	public void create(Question q){
		cuestion = new Element("cuestion");
		//staff.addContent(new Element("uri").setText(q.getUri()));
		cuestion.addContent(new Element("pregunta").setText(q.getSubject()));
		cuestion.addContent(new Element("contenido").setText(q.getContent()));
		//staff.addContent(new Element("maincat").setText(q.getMaincat()));
		cuestion.addContent(new Element("mejorRespuesta").setText(q.getBestAnswer()));
		respuestas = new Element("respuestas");
		for(int j=0; j<q.getAnswerItem().size(); j++){
			respuestas.addContent(new Element("respuesta").setText(q.getAnswerItem().get(j)));
		}
		cuestion.addContent(respuestas);
		cuestion.addContent(new Element("categoriaPrincipal").setText(q.getMaincat()));
		cuestion.addContent(new Element("subcategoria").setText(q.getSubcat()));
		cuestion.addContent(new Element("fecha").setText(q.getDate()));
		cuestion.addContent(new Element("resdate").setText(q.getResDate()));
		cuestion.addContent(new Element("votdate").setText(q.getVotDate()));
		cuestion.addContent(new Element("lastAnswerTS").setText(q.getLastAnswerTS()));
		//staff.addContent(new Element("qLang").setText(q.getqLang()));
		//staff.addContent(new Element("qINTL").setText(q.getqINTL()));
		cuestion.addContent(new Element("idioma").setText(q.getLanguage()));
		cuestion.addContent(new Element("id").setText(q.getId()));
		cuestion.addContent(new Element("mejorID").setText(q.getBestID()));
		doc.getRootElement().addContent(cuestion);
	}
	public void guardar(){
		try {
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileWriter("en-categoriaMusicEntertainment.xml"));
			System.out.println("File Saved!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}