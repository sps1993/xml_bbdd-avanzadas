package Dominio;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ReadXMLFile {
   public static void main(String argv[]) {
	   try {
		   SAXParserFactory factory = SAXParserFactory.newInstance();
		   SAXParser saxParser = factory.newSAXParser();
		   MyHandler handler = new MyHandler();
		   saxParser.parse("src/Dominio/FullOct2007.xml", handler);
	   } catch (Exception e) {
		   e.printStackTrace();
	   }
   }
}