<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html> 
			<head>
				<TITLE> Yahoo Answers </TITLE>
			</head>
			<body>
				<h1>Yahoo Answers</h1>
				<xsl:for-each select="preguntas/cuestion">
					<table border="2" width="100%">
						<tr>
							<td bgcolor="#13C3EC" width="15%"><strong>Pregunta</strong></td>
							<td><xsl:value-of select="pregunta"/></td>
						</tr>
						<tr>
							<td bgcolor="#F4FF00" width="15%"><strong>Contenido</strong></td>
							<td><xsl:value-of select="contenido"/></td>
						</tr>
						<tr>
							<td bgcolor="#FF8700" width="15%"><strong>Mejor Respuesta</strong></td>
							<td><xsl:value-of select="mejorRespuesta"/></td>
						</tr>
						<tr>
							<td bgcolor="#00FF63" colspan="2"><strong>Respuestas</strong></td>
						</tr>
						<xsl:for-each select="respuestas/respuesta">
							<tr>
								<td colspan="2"><xsl:value-of select="text()"/></td>
							</tr>
						</xsl:for-each>
						<tr>
							<td bgcolor="#00FFEE" width="15%"><strong>Categoria Principal</strong></td>
							<td><xsl:value-of select="categoriaPrincipal"/></td>
						</tr>
						<tr>
							<td bgcolor="#FF99DA" width="15%"><strong>Subcategoria</strong></td>
							<td><xsl:value-of select="subcategoria"/></td>
						</tr>
						<tr>
							<td bgcolor="#FF776F" width="15%"><strong>Idioma</strong></td>
							<td><xsl:value-of select="idioma"/></td>
						</tr>
						<tr>
							<td bgcolor="#00F99D" width="15%"><strong>ID Usuario Mejor Respuesta</strong></td>
							<td><xsl:value-of select="mejorID"/></td>
						</tr>
					</table>
					<br></br>
					<br></br>
					<br></br>
					<br></br>
					<br></br>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>